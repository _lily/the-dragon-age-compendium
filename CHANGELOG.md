# Changelog
-----
All notable changes to this project will be documented in this file.

## Unreleased
--------
### 0.0.0
* [Create basic pipeline for linting and testing](https://gitlab.com/_lily/the-dragon-age-compendium/-/merge_requests/32)
* [Update changelog; apply changes to settings.py](https://gitlab.com/_lily/the-dragon-age-compendium/-/merge_requests/33)