Closes #ENTER_ISSUE_NUMBER/copy_metadata #ENTER_ISSUE_NUMBER

## Summary
----------

## Checklist
* [ ] Link issue above
* [ ] Document any changes to the data contract
* [ ] Updated CHANGELOG.md
* [ ] Confirm unit tests have passed
* [ ] Squash commits on merge