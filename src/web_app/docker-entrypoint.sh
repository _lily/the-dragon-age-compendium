#!/bin/sh

# wait for Postgres to start
function postgres_ready(){
python << END
import sys
import os
import psycopg2

db_name = os.environ.get("POSTGRES_DB")
user = os.environ.get("POSTGRES_USER")
password = os.environ.get("POSTGRES_PASSWORD")
host = os.environ.get("POSTGRES_HOST")

try:
    conn = psycopg2.connect(dbname=db_name, user=user, password=password, host=host)
except psycopg2.OperationalError:
    sys.exit(-1)
sys.exit(0)
END
}

until postgres_ready; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

echo "Applying migrations..."
python manage.py migrate

exec "$@"