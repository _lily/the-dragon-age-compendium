We want to take output of our search functions and pass it to be rendered on the page:

https://stackoverflow.com/questions/4848611/rendering-a-template-variable-as-html

* user puts in search term
* back end finds that term, returns data in a certain format
* data is fed to the html code
* we can see the output from the user's query
    * this just made me think... is this something that needs to be different for each user?
    * like if user a and user B use site at same time and they are on the /search_results page, 
    is there a chance of collision?
* Bonus: highlight the search term in red text