Some potential SO posts:
* https://stackoverflow.com/questions/59251287/how-to-perform-full-text-search-in-django
* http://www.learningaboutelectronics.com/Articles/How-to-add-search-functionality-to-a-website-in-Django.php
* To save user input: https://stackoverflow.com/questions/32423401/save-form-data-in-django
    * https://stackoverflow.com/questions/59443512/how-to-store-every-search-term-to-database-in-django