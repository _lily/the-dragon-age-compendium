```
def download(request, id):
    obj = your_model_name.objects.get(id=id)
    filename = obj.model_attribute_name.path
    response = FileResponse(open(filename, 'rb'))
    return response
```
From: https://stackoverflow.com/questions/36392510/django-download-a-file
