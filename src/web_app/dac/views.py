from .models import DAO_TLK, DA2_TLK, DAI_TLK

from django.shortcuts import render
from django.views.generic import ListView, TemplateView

from itertools import chain

fake_examples = [
    {
        "name": "Flemeth",
        "game": "Dragon Age: Origins",
        "content": "Why dance when you can sing?",
        "date": "fake_date",
    },
    {
        "name": "Hawke",
        "game": "Dragon Age 2",
        "content": "I want to be a dragon",
        "date": "fake_date2",
    },
]


def home(request):
    context = {"quotes": fake_examples}
    # handles traffic from homepage
    return render(request, "dac/home.html", context)


def about(request):
    return render(request, "dac/about.html", {"title": "About"})


class SearchTlk(TemplateView):
    template_name = "dac/search.html"


class SearchView(ListView):
    template_name = "dac/test.html"
    count = 0

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["count"] = self.count or 0
        context["query"] = self.request.GET.get("q")
        return context

    def get_queryset(self):
        request = self.request
        query = request.GET.get("q", None)

        if query is not None:
            dao_results = DAO_TLK.objects.search(query)
            da2_results = DA2_TLK.objects.search(query)
            dai_results = DAI_TLK.objects.search(query)

            queryset_chain = chain(dao_results, da2_results, dai_results)

            qs = list(queryset_chain)
            self.count = len(qs)
            return qs
        return DAO_TLK.objects.none()


class SearchResults(ListView):

    template_name = "dac/search_results.html"

    def get_queryset(self):  # use Q if more complicated
        model = {
            "dao": DAO_TLK.objects.all(),
            "da2": DA2_TLK.objects.all(),
            "dai": DAI_TLK.objects.all(),
        }
        query = self.request.GET.getlist("q")
        game = self.request.GET.getlist("game")[-1]
        object_list = model[game].objects.filter(text__icontains=query)
        return object_list


def index(request):
    # could do something like top searches...
    # latest_question_list = Question.objects.order_by("-pub_date")[:5]
    # context = {"latest_question_list": latest_question_list}
    return render(request, "dac/index.html", {})


# Create your views here.
