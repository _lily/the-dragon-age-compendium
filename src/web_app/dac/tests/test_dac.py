from django.test import TestCase
from django.db import connection
from ..models import DAO_TLK, DA2_TLK, DAI_TLK
import json

"""
So far mostly database tests, making sure that database has not
been compromised

* split into functional tests too -- only run when you've migrated
"""


class TestDb(TestCase):
    def test_db_type(self):
        print(connection.vendor)
        assert connection.vendor == "postgresql"


class TestTlk(TestCase):

    fixtures = ["dao_tlk.json", "da2_tlk.json", "dai_tlk.json"]

    def test_Tlk(self):
        path = "dac/tests/data/sample_data.json"
        with open(path) as f:
            test_data = json.load(f)

        models = {"dao": DAO_TLK, "da2": DA2_TLK, "dai": DAI_TLK}

        for name, values in test_data.items():

            model = models[name]

            for entry in values:
                sid, text = entry

                if name == "dai":
                    # convert to decimal
                    sid = int(sid, 16)
                if not text:
                    continue
                model.objects.create(str_id=sid, text=text)

        new_model_sizes = [m.objects.count() for m in models.values()]

        assert new_model_sizes == [
            99918,
            43014,
            98293,
        ]

    def test_DAO_TLK(self):
        correct = 99908
        num_entries = DAO_TLK.objects.count()

        assert num_entries == correct

    def test_DA2_TLK(self):
        correct = 43004
        num_entries = len(DA2_TLK.objects.all())

        assert num_entries == correct

    def test_DAI_TLK(self):
        correct = 98283
        num_entries = DAI_TLK.objects.count()

        assert num_entries == correct


class TestConv(TestCase):
    def test_dao_cnv(self):
        pass

    def test_da2_cnv(self):
        pass

    def test_da_cnv(self):
        pass


class TestCodex(TestCase):
    def test_dao_codex(self):
        pass

    def test_da2_codex(self):
        pass

    def test_dai_codex(self):
        pass


class TestViews(TestCase):
    def test_one(self):
        pass
