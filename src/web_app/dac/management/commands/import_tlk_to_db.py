import json
from django.core.management import BaseCommand
from dac.models import DAO_TLK, DA2_TLK, DAI_TLK

"""
python manage.py import_tlk_to_db

--path /path/to/your/file.json

--game [dao, da2, dai]
"""


class Command(BaseCommand):
    help = "Load a tlk json file into the database"

    def add_arguments(self, parser):
        parser.add_argument("--path", type=str)
        parser.add_argument("--game", type=str)

    def handle(self, *args, **kwargs):
        path = kwargs["path"]
        game = kwargs["game"]

        with open(path) as f:
            tlk_json = json.load(f)

            db = {"dao": DAO_TLK, "da2": DA2_TLK, "dai": DAI_TLK}

            for sid, text in tlk_json.items():
                if game == "dai":
                    # convert to decimal
                    sid = int(sid, 16)
                if not text:
                    continue
                db[game].objects.create(str_id=sid, text=text)
