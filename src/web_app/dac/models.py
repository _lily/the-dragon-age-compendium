from django.db import models


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField("date published")

    def __str__(self):
        return self.question_text


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text


# Create your models here.
class Search(models.Model):
    # I think it would be cool to track what people lookup hehe
    search_key = models.CharField(max_length=100)
    # idk how to do this, but link it to which game they are looking up
    game_key = models.CharField(max_length=30)  # dai, da2, dao, all
    resource_key = models.CharField(max_length=10)  # character, codex, or both
    date_searched = models.DateTimeField(
        auto_now_add=True
    )  # I want to know when it was searched by the user


class SearchTlk(models.Model):
    search_key = models.CharField(max_length=100)
    game_key = models.CharField(max_length=30)
    date_searched = models.DateTimeField(auto_now_add=True)


class SearchManager(models.Manager):
    def search(self, query=None):
        qs = self.get_queryset()
        if query is not None:
            qs = qs.filter(text__icontains=query)
        return qs


"""
Tlk
"""


class Tlk(models.Model):
    str_id = models.IntegerField()
    text = models.CharField(
        max_length=3000
    )  # might need to up this or use a textfield

    objects = SearchManager()

    class Meta:
        abstract = True

    def __str__(self):
        return self.text


class DAO_TLK(Tlk):
    pass


class DA2_TLK(Tlk):
    pass


class DAI_TLK(Tlk):
    text = models.TextField()


"""
Conversations
"""


class Conversations(models.Model):
    pass


class DAO_Conv(Conversations):
    pass


class DA2_Conv(Conversations):
    pass


class DAI_Conv(Conversations):
    pass


"""
Codex
"""


class Codex(models.Model):
    pass


class DAO_Codex(Codex):
    pass


class DA2_Codex(Codex):
    pass


class DAI_Codex(Codex):
    pass
