from django import template
from django.utils.safestring import mark_safe

import re

register = template.Library()


@register.filter
def highlight_query(text, search):
    regex = re.compile(r"(?i){}".format(search))
    word_to_highlight = regex.search(text).group()

    if not word_to_highlight:
        # this might be unnecessary
        highlighted = text.replace(
            search, '<span class="highlight">{}</span>'.format(search)
        )
    else:
        term = word_to_highlight
        highlighted = text.replace(
            term, '<span class="highlight">{}</span>'.format(term)
        )

    return mark_safe(highlighted)
