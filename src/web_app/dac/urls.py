from django.urls import path

from . import views

urlpatterns = [
    path("", views.home, name="dac-home"),
    path("about/", views.about, name="dac-about"),
    path("index/", views.index, name="index"),
    path("search/", views.SearchTlk.as_view(), name="search"),
    path("test-search/", views.SearchView.as_view(), name="test-search"),
]
